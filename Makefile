## Show this help
help:
	echo "$(EMOJI_interrobang) Makefile version $(VERSION) help "
	echo ''
	echo 'About this help:'
	echo '  Commands are ${BLUE}blue${RESET}'
	echo '  Targets are ${YELLOW}yellow${RESET}'
	echo '  Descriptions are ${GREEN}green${RESET}'
	echo ''
	echo 'Usage:'
	echo '  ${BLUE}make${RESET} ${YELLOW}<target>${RESET}'
	echo ''
	echo 'Targets:'
	awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")+1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-${TARGET_MAX_CHAR_NUM}s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)


## Install all phars required with phive
phive-install:
	mkdir -p $$HOME/.phive/
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v $$HOME/.phive/:/tmp/phive/ -e PHIVE_HOME=/tmp/phive/ in2code/php:7.4-fpm phive install

## Run all Quality Assurance targets
qa-all: qa-lint-all qa-code-sniffer qa-mess-detector qa-phpstan-all

## Lint all languages
qa-lint-all: qa-lint-composer qa-lint-php-all

## Validate the composer.json schema
qa-lint-composer:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:7.4-fpm composer validate --strict

## PHP lint for all language levels
qa-lint-php-all: qa-lint-php-7.4 qa-lint-php-8.0 qa-lint-php-8.1 qa-lint-php-8.2

## PHP lint for language level 7.4
qa-lint-php-7.4:
	echo "$(EMOJI_digit_seven)$(EMOJI_digit_four) $(EMOJI_elephant) PHP lint 7.4"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:7.4-fpm bash -c "find . -path ./.Build -prune -false -o -type f -name '*.php' -print0 | xargs -0 -n1 -P$$(nproc) php -l -n > /dev/null"

## PHP lint for language level 8.0
qa-lint-php-8.0:
	echo "$(EMOJI_digit_seven)$(EMOJI_digit_four) $(EMOJI_elephant) PHP lint 8.0"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:8.0-fpm bash -c "find . -path ./.Build -prune -false -o -type f -name '*.php' -print0 | xargs -0 -n1 -P$$(nproc) php -l -n > /dev/null"

## PHP lint for language level 8.1
qa-lint-php-8.1:
	echo "$(EMOJI_digit_seven)$(EMOJI_digit_four) $(EMOJI_elephant) PHP lint 8.1"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:8.1-fpm bash -c "find . -path ./.Build -prune -false -o -type f -name '*.php' -print0 | xargs -0 -n1 -P$$(nproc) php -l -n > /dev/null"

## PHP lint for language level 8.2
qa-lint-php-8.2:
	echo "$(EMOJI_digit_seven)$(EMOJI_digit_four) $(EMOJI_elephant) PHP lint 8.2"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:8.2-fpm bash -c "find . -path ./.Build -prune -false -o -type f -name '*.php' -print0 | xargs -0 -n1 -P$$(nproc) php -l -n > /dev/null"

## PHP code sniffer
qa-code-sniffer:
	echo "$(EMOJI_pig_nose) PHP Code Sniffer"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:7.4-fpm .Build/phars/phpcs

## PHP
qa-fix-code-sniffer:
	echo "$(EMOJI_broom) PHP Code Beautifier and Fixer"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:7.4-fpm .Build/phars/phpcbf

## PHP mess detector
qa-mess-detector:
	echo "$(EMOJI_customs) PHP Mess Detector"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:7.4-fpm .Build/phars/phpmd Classes ansi .phpmd.xml

## PHP copy paste detector
qa-copy-paste-detector:
	echo "$(EMOJI_linked_paperlips) PHP Copy Paste Detector"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:7.4-fpm .Build/phars/phpcpd Classes

## Test with phpstan for all TYPO3 versions
qa-phpstan-all: qa-phpstan-t3v11 qa-phpstan-t3v12

## Test with phpstan for TYPO3 v11
qa-phpstan-t3v11:
	echo "$(EMOJI_nerd_face) PHPStan for TYPO3 v11"
	echo "$(EMOJI_hourglass_not_done) Preparing test environment"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/auth.json":/tmp/composer/auth.json -v "$$HOME/.composer/cache/":/tmp/composer/cache in2code/php:7.4-fpm composer update --prefer-lowest
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/auth.json":/tmp/composer/auth.json -v "$$HOME/.composer/cache/":/tmp/composer/cache in2code/php:7.4-fpm composer dumpautoload
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:7.4-fpm php -dmemory_limit=-1 .Build/phars/phpstan analyse -c .phpstan.neon

## Test with phpstan for TYPO3 v12
qa-phpstan-t3v12:
	echo "$(EMOJI_nerd_face) PHPStan for TYPO3 v12"
	echo "$(EMOJI_hourglass_not_done) Preparing test environment"
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/auth.json":/tmp/composer/auth.json -v "$$HOME/.composer/cache/":/tmp/composer/cache in2code/php:8.1-fpm composer update
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/auth.json":/tmp/composer/auth.json -v "$$HOME/.composer/cache/":/tmp/composer/cache in2code/php:8.1-fpm composer dumpautoload
	docker run --rm -it -u1000:1000 -v "$$PWD":/app in2code/php:8.1-fpm php -dmemory_limit=-1 .Build/phars/phpstan analyse -c .phpstan.neon

# SETTINGS
MAKEFLAGS += --silent
SHELL := /bin/bash
VERSION := 1.0.0

# COLORS
RED  := $(shell tput -Txterm setaf 1)
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
BLUE   := $(shell tput -Txterm setaf 4)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

# EMOJIS (some are padded right with whitespace for text alignment)
EMOJI_litter := "🚮️"
EMOJI_interrobang := "⁉️ "
EMOJI_floppy_disk := "💾️"
EMOJI_dividers := "🗂️ "
EMOJI_up := "🆙️"
EMOJI_receive := "📥️"
EMOJI_robot := "🤖️"
EMOJI_stop := "🛑️"
EMOJI_package := "📦️"
EMOJI_secure := "🔐️"
EMOJI_explodinghead := "🤯️"
EMOJI_rocket := "🚀️"
EMOJI_plug := "🔌️"
EMOJI_leftright := "↔️ "
EMOJI_upright := "↗️ "
EMOJI_thumbsup := "👍️"
EMOJI_telescope := "🔭️"
EMOJI_monkey := "🐒️"
EMOJI_elephant := "🐘️"
EMOJI_dolphin := "🐬️"
EMOJI_helicopter := "🚁️"
EMOJI_broom := "🧹"
EMOJI_nutandbolt := "🔩"
EMOJI_crystal_ball := "🔮"
EMOJI_nerd_face := "🤓"
EMOJI_digit_zero := "0️"
EMOJI_digit_one := "1️"
EMOJI_digit_two := "2️"
EMOJI_digit_three := "3️"
EMOJI_digit_four := "4️"
EMOJI_digit_seven := "7️"
EMOJI_pig_nose := "🐽"
EMOJI_customs := "🛃"
EMOJI_hot_face := "🥵"
EMOJI_cold_face := "🥶"
EMOJI_hourglass_not_done := "⏳"
EMOJI_linked_paperlips := "🖇️"
