<?php

declare(strict_types=1);

namespace CoStack\Api\ResponseFormatter;

use function in_array;

abstract class AbstractFormatter implements ResponseFormatter
{
    protected string $mime;

    protected $supportedMimes = [];

    public function supports(string $mime): bool
    {
        return in_array($mime, $this->supportedMimes);
    }

    public function setMime(string $mime): void
    {
        $this->mime = $mime;
    }

    public function getMime(): string
    {
        return $this->mime;
    }
}
