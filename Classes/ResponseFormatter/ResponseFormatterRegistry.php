<?php

declare(strict_types=1);

namespace CoStack\Api\ResponseFormatter;

use TYPO3\CMS\Core\SingletonInterface;

class ResponseFormatterRegistry implements SingletonInterface
{
    /** @var array<ResponseFormatter> */
    private array $responseFormatters = [];

    public function register(ResponseFormatter $responseFormatter): void
    {
        $this->responseFormatters[] = $responseFormatter;
    }

    public function getFormatterForMimeType(array $mimes): ?ResponseFormatter
    {
        foreach ($mimes as $mime) {
            foreach ($this->responseFormatters as $responseFormatter) {
                if ($responseFormatter->supports($mime)) {
                    $responseFormatter->setMime($mime);
                    return $responseFormatter;
                }
            }
        }
        return null;
    }
}
