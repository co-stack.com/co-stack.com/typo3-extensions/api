<?php

declare(strict_types=1);

namespace CoStack\Api\ResponseFormatter;

use function json_encode;

use const JSON_THROW_ON_ERROR;

class JsonFormatter extends AbstractFormatter
{
    protected $supportedMimes = [
        'application/json',
    ];

    public function process(array $data): string
    {
        return json_encode($data, JSON_THROW_ON_ERROR);
    }
}
