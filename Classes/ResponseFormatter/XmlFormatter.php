<?php

declare(strict_types=1);

namespace CoStack\Api\ResponseFormatter;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class XmlFormatter extends AbstractFormatter
{
    protected $supportedMimes = [
        'application/xml',
    ];

    public function process(array $data): string
    {
        return '<?xml version="1.0" encoding="utf-8" standalone="yes"?>' . "\n" . GeneralUtility::array2xml($data);
    }
}
