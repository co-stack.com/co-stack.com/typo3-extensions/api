<?php

declare(strict_types=1);

namespace CoStack\Api\ResponseFormatter;

use function array_keys;
use function fclose;
use function fopen;
use function fputcsv;
use function fseek;
use function stream_get_contents;

class CsvFormatter extends AbstractFormatter
{
    protected $supportedMimes = [
        'application/csv',
    ];

    public function process(array $data): string
    {
        $stream = fopen('php://temp', 'wb');
        $header = array_keys($data);
        fputcsv($stream, $header);
        foreach ($data as $value) {
            fputcsv($stream, $value);
        }
        fseek($stream, 0);
        $content = stream_get_contents($stream);
        fclose($stream);
        return $content;
    }
}
