<?php

declare(strict_types=1);

namespace CoStack\Api\ResponseFormatter;

interface ResponseFormatter
{
    public function supports(string $mime): bool;

    public function setMime(string $mime): void;

    public function getMime(): string;

    public function process(array $data): string;
}
