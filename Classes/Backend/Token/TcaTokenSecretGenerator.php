<?php

declare(strict_types=1);

namespace CoStack\Api\Backend\Token;

use Exception;
use TYPO3\CMS\Core\DataHandling\DataHandler;

use function array_keys;
use function hash;
use function is_string;
use function random_bytes;
use function strpos;

class TcaTokenSecretGenerator
{
    /**
     * @param DataHandler $dataHandler
     *
     * @throws Exception
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // phpcs:disable
    public function processDatamap_beforeStart(DataHandler $dataHandler): void
    {
        // phpcs:enable
        foreach (array_keys($dataHandler->datamap['tx_api_domain_model_token'] ?? []) as $uid) {
            if (is_string($uid) && 0 === strpos($uid, 'NEW')) {
                $dataHandler->datamap['tx_api_domain_model_token'][$uid]['secret'] = hash('sha256', random_bytes(256));
            }
        }
    }
}
