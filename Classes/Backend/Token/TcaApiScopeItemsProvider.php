<?php

declare(strict_types=1);

namespace CoStack\Api\Backend\Token;

use CoStack\Api\Service\ApiRouteConfigService;

use function array_filter;

class TcaApiScopeItemsProvider
{
    private ApiRouteConfigService $apiRouteConfigService;

    public function __construct(ApiRouteConfigService $apiRouteConfigService)
    {
        $this->apiRouteConfigService = $apiRouteConfigService;
    }

    /**
     * @param array{items: array<int, array{0: string, 1: string}>} $config
     */
    public function getItems(array &$config): void
    {
        $packageRoutes = $this->apiRouteConfigService->getRoutes();

        $filterPublicRoutes = static function (array $route): bool {
            return !$route['public'];
        };

        foreach ($packageRoutes as $package => $routes) {
            $privateRoutes = array_filter($routes, $filterPublicRoutes);

            if (!empty($privateRoutes)) {
                $config['items'][] = ['  -- EXT:' . $package . ' -- ', '--div--'];
                foreach ($privateRoutes as $path => $route) {
                    $config['items'][] = [$package . $path, $package . $path];
                }
            }
        }
    }
}
