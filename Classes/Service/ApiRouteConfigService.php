<?php

declare(strict_types=1);

namespace CoStack\Api\Service;

use CoStack\Api\Api\Api;
use TYPO3\CMS\Core\Package\Exception\UnknownPackageException;
use TYPO3\CMS\Core\Package\PackageManager;

use function array_reduce;
use function file_exists;
use function trim;

class ApiRouteConfigService
{
    private const GLOBAL_DEFAULTS = [
        'public' => false,
        'type' => Api::TYPE_EARLY,
        'mimes' => [],
    ];
    private PackageManager $packageManager;

    public function __construct(PackageManager $packageManager)
    {
        $this->packageManager = $packageManager;
    }

    public function getRoutes(): array
    {
        $packages = $this->packageManager->getActivePackages();
        $routes = [];
        foreach ($packages as $package) {
            $filePath = $package->getPackagePath() . 'Configuration/Api.php';
            if (file_exists($filePath)) {
                $routesConfig = require($filePath);
                $routes[$package->getPackageKey()] = $this->normalizeRoutes($routesConfig);
            }
        }
        return $routes;
    }

    /**
     * @throws UnknownPackageException
     */
    public function getRoutesForPackage(string $packageKey): array
    {
        $package = $this->packageManager->getPackage($packageKey);
        $filePath = $package->getPackagePath() . 'Configuration/Api.php';
        if (file_exists($filePath)) {
            $routes = require($filePath);
            return $this->normalizeRoutes($routes);
        }
        return [];
    }

    private function normalizeRoutes(array $config): array
    {
        $routeDefaults = [];
        if (isset($config['_default'])) {
            $routeDefaults = $config['_default'];
            unset($config['_default']);
        }
        foreach ($config as $route => $settings) {
            $settingsArray = [
                self::GLOBAL_DEFAULTS,
            ];
            foreach ($routeDefaults as $path => $defaults) {
                if (0 === strpos($route, $path)) {
                    $settingsArray[] = $defaults;
                }
            }
            $settingsArray[] = $settings;

            $settings = array_reduce($settingsArray, 'array_merge', []);
            unset($config[$route]);
            $config['/' . trim($route, '/')] = $settings;
        }
        return $config;
    }
}
