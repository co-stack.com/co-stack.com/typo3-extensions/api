<?php

declare(strict_types=1);

namespace CoStack\Api;

use Exception;

abstract class ApiException extends Exception
{
}
