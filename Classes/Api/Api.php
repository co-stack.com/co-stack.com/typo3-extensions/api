<?php

declare(strict_types=1);

namespace CoStack\Api\Api;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface Api
{
    public const TYPE_EARLY = 'early';
    public const TYPE_FRONTEND = 'frontend';

    /**
     * The handle method is designed to be as simple as possible.
     * Other than middlewares, it does not return a response.
     * Instead, the processing result of the API call is returned as an array.
     * The API service will then format the result accordingly to the request headers (Content-Accept) or request format
     * (.xml, .csv, ...)
     *
     * @param ServerRequestInterface $request
     *
     * @return array|ResponseInterface
     */
    public function handle(ServerRequestInterface $request);
}
