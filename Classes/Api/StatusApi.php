<?php

declare(strict_types=1);

namespace CoStack\Api\Api;

use Psr\Http\Message\ServerRequestInterface;

class StatusApi implements Api
{
    public function handle(ServerRequestInterface $request): array
    {
        return ['status' => true];
    }
}
