<?php

declare(strict_types=1);

namespace CoStack\Api\Api;

use CoStack\Api\Service\ApiRouteConfigService;
use Psr\Http\Message\ServerRequestInterface;

use function array_keys;

class DiscoveryApi implements Api
{
    private ApiRouteConfigService $apiRouteConfigService;

    public function __construct(ApiRouteConfigService $apiRouteConfigService)
    {
        $this->apiRouteConfigService = $apiRouteConfigService;
    }

    public function handle(ServerRequestInterface $request): array
    {
        $config = $this->apiRouteConfigService->getRoutes();

        $targetList = [];
        foreach ($config as $package => $targets) {
            foreach (array_keys($targets) as $target) {
                $targetList[] = $package . $target;
            }
        }
        return ['targets' => $targetList];
    }
}
