<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Exception;

use CoStack\Api\ApiException;

class ApiRouteNotFoundException extends ApiException
{
    public function __construct()
    {
        parent::__construct('NOT AN ERROR', 1702054655);
    }
}
