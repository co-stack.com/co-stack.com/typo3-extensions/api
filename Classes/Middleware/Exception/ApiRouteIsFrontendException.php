<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Exception;

use CoStack\Api\ApiException;
use CoStack\Api\Routing\Route;

class ApiRouteIsFrontendException extends ApiException
{
    private Route $route;

    public function __construct(Route $route)
    {
        $this->route = $route;
        parent::__construct('NOT AN ERROR', 1702054629);
    }

    public function getRoute(): Route
    {
        return $this->route;
    }
}
