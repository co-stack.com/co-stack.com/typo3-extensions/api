<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Api;

use CoStack\Api\Api\Api;
use CoStack\Api\Routing\Route;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use ReflectionClass;
use Throwable;
use TYPO3\CMS\Core\Error\ExceptionHandlerInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

use function sprintf;

class ApiExecutorMiddleware implements MiddlewareInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private const STATUS_INTERNAL_SERVER_ERROR = 500;
    private const MESSAGE_INTERNAL_SERVER_ERROR = "Internal Server Error\n";
    private ResponseFactoryInterface $responseFactory;

    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var Route $route */
        $route = $request->getAttribute('route');

        /** @var Api $apiObject */
        $apiObject = GeneralUtility::makeInstance($route->getClass());
        try {
            $response = $apiObject->handle($request);
            if ($response instanceof ResponseInterface) {
                return $response;
            }
            $request = $request->withAttribute('data', $response);
        } catch (Throwable $throwable) {
            $this->handleException($request, $route, $throwable);
            $response = $this->responseFactory->createResponse();
            $response->getBody()->write(self::MESSAGE_INTERNAL_SERVER_ERROR);
            return $response->withStatus(self::STATUS_INTERNAL_SERVER_ERROR);
        }

        return $handler->handle($request);
    }

    protected function handleException(ServerRequestInterface $request, Route $route, Throwable $throwable): void
    {
        $systemConfiguration = $this->getSystemConfiguration();
        $displayErrorsSetting = (int)$systemConfiguration['displayErrors'];
        $devIpMask = $systemConfiguration['devIPmask'];
        $remoteAddress = GeneralUtility::getIndpEnv('REMOTE_ADDR');
        $ipMatchesDevSystem = GeneralUtility::cmpIP($remoteAddress, $devIpMask);

        if ($displayErrorsSetting === 1 || (($displayErrorsSetting === -1) && $ipMatchesDevSystem)) {
            $reflection = new ReflectionClass($systemConfiguration['debugExceptionHandler']);
            /** @var ExceptionHandlerInterface $exceptionHandler */
            $exceptionHandler = $reflection->newInstanceWithoutConstructor();
            if ($request->getHeaderLine('X-T3API-SAPI') === 'cli') {
                $exceptionHandler->echoExceptionCLI($throwable);
                return;
            }
            $exceptionHandler->handleException($throwable);
            return;
        }

        $args = $route->getPath();
        $class = $route->getClass();
        $message = sprintf('API call to route "%s" (class "%s") failed with exception', $args, $class);
        $this->logger->critical($message, ['exception' => $throwable]);
    }

    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    protected function getSystemConfiguration(): array
    {
        return $GLOBALS['TYPO3_CONF_VARS']['SYS'];
    }
}
