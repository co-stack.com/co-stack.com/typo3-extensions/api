<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Api;

use CoStack\Api\ResponseFormatter\ResponseFormatterRegistry;
use CoStack\Api\Routing\Route;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\Mime\MimeTypes;
use TYPO3\CMS\Core\Utility\GeneralUtility;

use function array_column;
use function array_multisort;
use function count;
use function explode;
use function strpos;

use const SORT_DESC;

class ResponseFormatterMiddleware implements MiddlewareInterface
{
    private const STATUS_NOT_ACCEPTABLE = 406;
    private const MESSAGE_NOT_ACCEPTABLE = 'HTTP 406 Not Acceptable';
    private ResponseFormatterRegistry $formatterRegistry;
    private ResponseFactoryInterface $responseFactory;
    private MimeTypes $mimes;

    public function __construct(ResponseFormatterRegistry $formatterRegistry, ResponseFactoryInterface $responseFactory)
    {
        $this->formatterRegistry = $formatterRegistry;
        $this->responseFactory = $responseFactory;
        $this->mimes = new MimeTypes();
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $mimes = $this->determineMimeTypes($request);

        $formatter = $this->formatterRegistry->getFormatterForMimeType($mimes);
        if (null === $formatter) {
            $response = $this->responseFactory->createResponse();
            $response->getBody()->write(self::MESSAGE_NOT_ACCEPTABLE);
            return $response->withStatus(self::STATUS_NOT_ACCEPTABLE);
        }

        $data = $request->getAttribute('data');
        $formattedData = $formatter->process($data);
        $response = $this->responseFactory->createResponse();
        $response->getBody()->write($formattedData);
        // @phpstan-ignore-next-line
        return $response->withHeader('Content-Type', $formatter->getMime());
    }

    protected function determineMimeTypes(ServerRequestInterface $request): array
    {
        /** @var Route $route */
        $route = $request->getAttribute('route');
        if ($route->hasExt()) {
            return $this->mimes->getMimeTypes($route->getExt());
        }

        $accepted = $this->getAcceptedMimes($request);
        if (!empty($accepted)) {
            return $accepted;
        }

        return $route->getMimes();
    }

    public function getAcceptedMimes(ServerRequestInterface $request): array
    {
        $acceptHeaders = $this->parseAcceptedMimes($request);

        $priorities = array_column($acceptHeaders, 'q');
        array_multisort($priorities, SORT_DESC, $acceptHeaders);

        return array_column($acceptHeaders, 'mime');
    }

    protected function parseAcceptedMimes(ServerRequestInterface $request): array
    {
        $parsedAcceptHeaders = [];
        if (!$request->hasHeader('Accept')) {
            return $parsedAcceptHeaders;
        }

        $acceptHeaders = $request->getHeader('Accept');
        foreach ($acceptHeaders as $acceptHeader) {
            if ($acceptHeader === '*/*') {
                continue;
            }
            $parsedAcceptHeaders = $this->parseAcceptHeader($acceptHeader, $parsedAcceptHeaders);
        }
        return $parsedAcceptHeaders;
    }

    protected function parseAcceptHeader(string $acceptHeader, array $acceptHeaders): array
    {
        $orderedHeaderValues = GeneralUtility::trimExplode(',', $acceptHeader, true);
        $headerValuesCount = count($orderedHeaderValues);
        foreach ($orderedHeaderValues as $index => $acceptHeaderParts) {
            $acceptHeaders[] = $this->parseFormat($acceptHeaderParts, $headerValuesCount, $index);
        }
        return $acceptHeaders;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    protected function parseFormat(string $acceptHeaderParts, int $headerValuesCount, int $index): array
    {
        $acceptHeaderSubParts = GeneralUtility::trimExplode(';', $acceptHeaderParts, true);
        foreach ($acceptHeaderSubParts as $pathPart) {
            if (false !== strpos($pathPart, '=')) {
                [$key, $value] = explode('=', $pathPart, 2);
                $format[$key] = $value;
            } else {
                $format['mime'] = $pathPart;
            }
            $extensions = $this->mimes->getExtensions($format['mime']);
            $format['ext'] = $extensions[0] ?? '';
        }
        if (!isset($format['q'])) {
            $format['q'] = $headerValuesCount - $index;
        }
        return $format;
    }
}
