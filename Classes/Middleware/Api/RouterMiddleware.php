<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Api;

use CoStack\Api\Api\Api;
use CoStack\Api\Middleware\Exception\ApiRouteIsFrontendException;
use CoStack\Api\Middleware\Exception\ApiRouteNotFoundException;
use CoStack\Api\Routing\Route;
use CoStack\Api\Routing\Router;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RouterMiddleware implements MiddlewareInterface
{
    private Router $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // If the route has already been matched "early", we don't need to re-identify it here.
        $route = $request->getAttribute('api-route');
        if ($route instanceof Route) {
            $request = $request->withAttribute('route', $route);
            return $handler->handle($request);
        }
        $route = $this->router->match($request);

        if (null === $route) {
            throw new ApiRouteNotFoundException();
        }
        if (Api::TYPE_FRONTEND === $route->getType()) {
            throw new ApiRouteIsFrontendException($route);
        }

        $request = $request->withAttribute('route', $route);
        return $handler->handle($request);
    }
}
