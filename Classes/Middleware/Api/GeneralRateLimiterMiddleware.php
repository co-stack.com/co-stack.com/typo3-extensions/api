<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Api;

use Closure;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Http\NormalizedParams;

use function array_filter;
use function array_slice;

class GeneralRateLimiterMiddleware implements MiddlewareInterface
{
    private const STATUS_TOO_MANY_REQUESTS = 429;
    private const MESSAGE_TOO_MANY_REQUESTS = "HTTP 429 Too Many Requests\n";
    private ResponseFactoryInterface $responseFactory;
    private FrontendInterface $cache;
    private array $extConf;

    public function __construct(ResponseFactoryInterface $responseFactory, FrontendInterface $fastCache, array $extConf)
    {
        $this->responseFactory = $responseFactory;
        $this->cache = $fastCache;
        $this->extConf = $extConf;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->requestExceedsRateLimit($request)) {
            $response = $this->responseFactory->createResponse();
            $response->getBody()->write(self::MESSAGE_TOO_MANY_REQUESTS);
            return $response->withStatus(self::STATUS_TOO_MANY_REQUESTS);
        }
        return $handler->handle($request);
    }

    private function requestExceedsRateLimit(ServerRequestInterface $request): bool
    {
        $maxRequests = (int)$this->extConf['requests'];

        if ($maxRequests === 0) {
            return false;
        }

        /** @var NormalizedParams $normalizedParams */
        $normalizedParams = $request->getAttribute('normalizedParams');
        $ipHash = hash('sha1', $normalizedParams->getRemoteAddress());

        $reqs = [];
        if ($this->cache->has($ipHash)) {
            $reqs = $this->cache->get($ipHash);
        }

        // Remove requests that exceed the max request limit to prevent cache flooding
        $reqs = array_slice($reqs, -($maxRequests + 1));
        // Remove requests that are too old to be considered
        $lowestTimestamp = $this->getExecTime() - (int)$this->extConf['time_frame'];
        $reqs = array_filter($reqs, $this->getRequestTimeFilter($lowestTimestamp));

        $reqs[] = $this->getExecTime();
        $this->cache->set($ipHash, $reqs);

        return count($reqs) > $maxRequests;
    }

    private function getRequestTimeFilter(int $lowestTimestamp): Closure
    {
        return static function (int $timestamp) use ($lowestTimestamp): bool {
            return $timestamp > $lowestTimestamp;
        };
    }

    /**
     * @return int
     *
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    protected function getExecTime(): int
    {
        return $GLOBALS['EXEC_TIME'];
    }
}
