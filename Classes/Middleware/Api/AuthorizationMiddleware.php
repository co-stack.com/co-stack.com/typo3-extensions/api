<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Api;

use CoStack\Api\Routing\Route;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Http\NormalizedParams;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class AuthorizationMiddleware implements MiddlewareInterface
{
    private const STATUS_FORBIDDEN = 403;
    private const MESSAGE_FORBIDDEN = "HTTP 403 Forbidden\n";
    private const TABLE_TOKEN = 'tx_api_domain_model_token';
    public const REQ_HEADER_TOKEN = 'X-T3API-TOKEN';
    public const REQ_ATTR_AUTHORIZATION = 'authorization';
    public const REQ_ATTR_TOKEN = 'token';
    public const REQ_AUTH_TOKEN = 'token';
    public const REQ_AUTH_PUBLIC = 'public';
    private ResponseFactoryInterface $responseFactory;
    private ConnectionPool $connectionPool;

    public function __construct(ResponseFactoryInterface $responseFactory, ConnectionPool $connectionPool)
    {
        $this->responseFactory = $responseFactory;
        $this->connectionPool = $connectionPool;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $request = $this->getAuthorizedRequest($request);
        if (null === $request) {
            $response = $this->responseFactory->createResponse();
            $response->getBody()->write(self::MESSAGE_FORBIDDEN);
            return $response->withStatus(self::STATUS_FORBIDDEN);
        }

        return $handler->handle($request);
    }

    private function getAuthorizedRequest(ServerRequestInterface $request): ?ServerRequestInterface
    {
        /** @var Route $route */
        $route = $request->getAttribute('route');
        if ($route->isPublic()) {
            return $request->withAttribute(self::REQ_ATTR_AUTHORIZATION, self::REQ_AUTH_PUBLIC);
        }
        $token = $request->getHeaderLine(self::REQ_HEADER_TOKEN);
        if ('' === $token) {
            $token = $request->getQueryParams()['token'] ?? '';
        }
        /** @var NormalizedParams $normalizedParams */
        $normalizedParams = $request->getAttribute('normalizedParams');
        if (!empty($token)) {
            $query = $this->connectionPool->getQueryBuilderForTable(self::TABLE_TOKEN);
            $query->select('*')
                  ->from(self::TABLE_TOKEN)
                  ->where($query->expr()->eq('secret', $query->createNamedParameter($token)))
                  ->setMaxResults(1);
            $statement = $query->executeQuery();
            $row = $statement->fetchAssociative();
            // start-time, end-time, delete and disable restrictions are done via the default restriction container
            if (
                !empty($row)
                && $route->isInScopeList($row['scope'])
                && GeneralUtility::cmpIP($normalizedParams->getRemoteAddress(), $row['request_cidr'])
            ) {
                $request = $request->withAttribute(self::REQ_ATTR_AUTHORIZATION, self::REQ_AUTH_TOKEN);
                return $request->withAttribute(self::REQ_ATTR_TOKEN, $row);
            }
        }
        return null;
    }
}
