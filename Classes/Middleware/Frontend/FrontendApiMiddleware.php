<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Frontend;

use CoStack\Api\Routing\Route;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FrontendApiMiddleware implements MiddlewareInterface
{
    private RequestHandlerInterface $requestHandler;

    public function __construct(RequestHandlerInterface $requestHandler)
    {
        $this->requestHandler = $requestHandler;
    }
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $route = $request->getAttribute('api-route');
        if (!$route instanceof Route) {
            return $handler->handle($request);
        }
        return $this->requestHandler->handle($request);
    }
}
