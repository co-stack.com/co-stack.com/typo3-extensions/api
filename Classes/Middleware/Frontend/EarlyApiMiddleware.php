<?php

declare(strict_types=1);

namespace CoStack\Api\Middleware\Frontend;

use CoStack\Api\Middleware\Exception\ApiRouteIsFrontendException;
use CoStack\Api\Middleware\Exception\ApiRouteNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function substr;

class EarlyApiMiddleware implements MiddlewareInterface
{
    private RequestHandlerInterface $requestHandler;

    public function __construct(RequestHandlerInterface $requestHandler)
    {
        $this->requestHandler = $requestHandler;
    }

    /**
     * @noinspection PhpRedundantCatchClauseInspection
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Early return for performance.
        $path = substr($request->getUri()->getPath(), 0, 5);
        if ($path !== '/api/') {
            return $handler->handle($request);
        }

        try {
            return $this->requestHandler->handle($request);
        } catch (ApiRouteNotFoundException $exception) {
            return $handler->handle($request);
        } catch (ApiRouteIsFrontendException $exception) {
            $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFoundOnCHashError'] = false;
            $request = $request->withAttribute('noCache', true);
            $request = $request->withAttribute('api-route', $exception->getRoute());
            return $handler->handle($request);
        }
    }
}
