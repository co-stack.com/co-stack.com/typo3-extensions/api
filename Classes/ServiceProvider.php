<?php

declare(strict_types=1);

namespace CoStack\Api;

use ArrayObject;
use CoStack\Api\Http\Api\DefaultRequestHandler;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\MiddlewareDispatcher;
use TYPO3\CMS\Core\Http\MiddlewareStackResolver;
use TYPO3\CMS\Core\Package\AbstractServiceProvider;

class ServiceProvider extends AbstractServiceProvider
{
    protected static function getPackagePath(): string
    {
        return __DIR__ . '/../';
    }

    public function getFactories(): array
    {
        return [
            'co-stack.api.middlewares' => [static::class, 'getApiMiddlewares'],
            'co-stack.api.request.handler' => [static::class, 'getRequestHandler'],
        ];
    }

    public static function getApiMiddlewares(ContainerInterface $container): iterable
    {
        return new ArrayObject($container->get(MiddlewareStackResolver::class)->resolve('api'));
    }

    public static function getRequestHandler(ContainerInterface $container): RequestHandlerInterface
    {
        return new MiddlewareDispatcher(
            $container->get(DefaultRequestHandler::class),
            $container->get('co-stack.api.middlewares'),
            $container
        );
    }

    protected static function getPackageName(): string
    {
        return 'co-stack/api';
    }
}
