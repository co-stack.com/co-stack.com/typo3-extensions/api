<?php

declare(strict_types=1);

namespace CoStack\Api\DependencyInjection;

use CoStack\Api\ResponseFormatter\ResponseFormatterRegistry;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

use function array_keys;

class ResponseFormatterPass implements CompilerPassInterface
{
    private string $tagName;

    public function __construct(string $tagName)
    {
        $this->tagName = $tagName;
    }

    public function process(ContainerBuilder $container): void
    {
        $formatterRegistry = $container->getDefinition(ResponseFormatterRegistry::class);

        $taggedServiceIds = $container->findTaggedServiceIds($this->tagName);
        foreach (array_keys($taggedServiceIds) as $id) {
            $definition = $container->findDefinition($id);
            if (!$definition->isAutoconfigured() || $definition->isAbstract()) {
                continue;
            }

            $formatterRegistry->addMethodCall(
                'register',
                [new Reference($id)]
            );
        }
    }
}
