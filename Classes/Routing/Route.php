<?php

declare(strict_types=1);

namespace CoStack\Api\Routing;

use TYPO3\CMS\Core\Utility\GeneralUtility;

use function in_array;

class Route
{
    private string $class;
    private string $ext;
    private string $package;
    private string $path;
    private bool $public;
    private array $mimes;
    private string $type;

    public function __construct(
        string $class,
        string $ext,
        string $package,
        string $path,
        bool $public,
        array $mimes,
        string $type
    ) {
        $this->class = $class;
        $this->ext = $ext;
        $this->package = $package;
        $this->path = $path;
        $this->public = $public;
        $this->mimes = $mimes;
        $this->type = $type;
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function hasExt(): bool
    {
        return !empty($this->ext);
    }

    public function getExt(): string
    {
        return $this->ext;
    }

    public function getPackage(): string
    {
        return $this->package;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function hasMimes(): bool
    {
        return !empty($this->mimes);
    }

    public function getMimes(): array
    {
        return $this->mimes;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isInScopeList(string $scopeList): bool
    {
        $scopeArray = GeneralUtility::trimExplode(',', $scopeList);
        $currentScope = $this->package . $this->path;
        return in_array($currentScope, $scopeArray);
    }
}
