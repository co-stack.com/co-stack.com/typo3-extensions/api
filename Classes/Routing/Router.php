<?php

declare(strict_types=1);

namespace CoStack\Api\Routing;

use CoStack\Api\Service\ApiRouteConfigService;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Package\Exception\UnknownPackageException;

use function array_merge;
use function preg_match;

class Router
{
    private const API_PATH_REGEX = '/^
                    \/api                                   # match api
                    \/(?P<package>(?:[\w\-\_\%\[\]]+))    # match package name
                    (?P<path>(?:\/[\w\-\_\%\[\]]+)*)     # match any subsequent path part
                    (\.(?P<ext>[\w]+)|\/)?                # match an optional extension or an ending slash
                $/x';
    private ApiRouteConfigService $apiRouteConfigService;

    public function __construct(ApiRouteConfigService $apiRouteConfigService)
    {
        $this->apiRouteConfigService = $apiRouteConfigService;
    }

    public function match(ServerRequestInterface $request): ?Route
    {
        $path = $request->getUri()->getPath();
        $pathMatches = [];
        if (1 !== preg_match(self::API_PATH_REGEX, $path, $pathMatches)) {
            return null;
        }

        $params = [];
        $params['package'] = $pathMatches['package'] ?: '/';
        $params['path'] = $pathMatches['path'] ?: '/';
        $params['ext'] = $pathMatches['ext'] ?? '';

        try {
            $routes = $this->apiRouteConfigService->getRoutesForPackage($params['package']);
        } catch (UnknownPackageException $exception) {
            return null;
        }

        if (empty($routes[$params['path']])) {
            return null;
        }

        $matchedRoute = $routes[$params['path']];
        $routeParams = array_merge($params, $matchedRoute);

        return new Route(
            $routeParams['class'],
            $routeParams['ext'],
            $routeParams['package'],
            $routeParams['path'],
            $routeParams['public'],
            $routeParams['mimes'] ?? [],
            $routeParams['type'],
        );
    }
}
