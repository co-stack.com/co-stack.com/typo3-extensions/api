<?php

declare(strict_types=1);

namespace CoStack\ApiTests\Unit\Api;

use CoStack\Api\Api\StatusApi;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * @coversDefaultClass \CoStack\Api\Api\StatusApi
 */
class StatusApiTest extends UnitTestCase
{
    /**
     * @covers ::handle
     */
    public function testStatusApiReturnsCorrectResponse(): void
    {
        $statusApi = new StatusApi();
        $response = $statusApi->handle(new ServerRequest());
        $this->assertSame(['status' => true], $response);
    }
}
