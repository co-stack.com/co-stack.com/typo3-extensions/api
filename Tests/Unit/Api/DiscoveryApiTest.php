<?php

declare(strict_types=1);

namespace CoStack\ApiTests\Unit\Api;

use CoStack\Api\Api\DiscoveryApi;
use CoStack\Api\Api\StatusApi;
use CoStack\Api\Service\ApiRouteConfigService;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * @coversDefaultClass \CoStack\Api\Api\DiscoveryApi
 */
class DiscoveryApiTest extends UnitTestCase
{
    /**
     * @covers ::handle
     */
    public function testDiscoveryApiReturnsCorrectTargets(): void
    {
        $routes = [
            'test_package' => [
                '/status' => [
                    'class' => StatusApi::class,
                    'public' => true,
                ],
            ],
        ];
        $expected = [
            'targets' => [
                'test_package/status',
            ],
        ];

        $apiRouteConfigService = $this->createMock(ApiRouteConfigService::class);
        $apiRouteConfigService->method('getRoutes')->willReturn($routes);
        $discoveryApi = new DiscoveryApi($apiRouteConfigService);

        $targets = $discoveryApi->handle(new ServerRequest());

        $this->assertSame($expected, $targets);
    }
}
