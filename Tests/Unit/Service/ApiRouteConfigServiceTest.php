<?php

declare(strict_types=1);

namespace CoStack\ApiTests\Unit\Service;

use CoStack\Api\Api\Api;
use CoStack\Api\Api\StatusApi;
use CoStack\Api\Service\ApiRouteConfigService;
use org\bovigo\vfs\vfsStream;
use TYPO3\CMS\Core\Package\PackageInterface;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * @coversDefaultClass \CoStack\Api\Service\ApiRouteConfigService
 */
class ApiRouteConfigServiceTest extends UnitTestCase
{
    // test that the subject returns the expected routes for a package
    public function testGetRoutesReturnsExpectedRoutes(): void
    {
        $expected = [
            '/status' => [
                'public' => true,
                'type' => Api::TYPE_EARLY,
                'mimes' => [],
                'class' => StatusApi::class,
            ],
        ];

        $structure = [
            'Configuration' => [
                'Api.php' => <<<PHP
<?php
return [
    '/status' => [
        'class' => \CoStack\Api\Api\StatusApi::class,
        'public' => true,
    ],
];
PHP
                ,
            ],
        ];
        $root = vfsStream::setup('root', null, $structure);
        $packagePath = $root->url() . '/';

        $package = $this->createMock(PackageInterface::class);
        $package->method('getPackagePath')->willReturn($packagePath);

        $packageManager = $this->createMock(PackageManager::class);
        $packageManager->method('getPackage')->willReturn($package);

        $apiRouteConfigService = new ApiRouteConfigService($packageManager);
        $routes = $apiRouteConfigService->getRoutesForPackage('test_package');

        $this->assertSame($expected, $routes);
    }
}
