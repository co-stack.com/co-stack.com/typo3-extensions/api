<?php

declare(strict_types=1);

namespace CoStack\ApiTests\Unit\Routing;

use CoStack\Api\Api\Api;
use CoStack\Api\Api\StatusApi;
use CoStack\Api\Routing\Router;
use CoStack\Api\Service\ApiRouteConfigService;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\CMS\Core\Http\Uri;
use TYPO3\CMS\Core\Package\Exception\UnknownPackageException;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * @coversDefaultClass \CoStack\Api\Routing\Router
 */
class RouterTest extends UnitTestCase
{
    /**
     * @covers ::match
     */
    public function testRouterCreatesRouteForMatchingRequest(): void
    {
        $routes = [
            '/status' => [
                'class' => StatusApi::class,
                'public' => true,
                'type' => Api::TYPE_EARLY,
            ],
        ];

        $apiRouteConfigService = $this->createMock(ApiRouteConfigService::class);
        $apiRouteConfigService->method('getRoutesForPackage')->willReturn($routes);

        $uri = new Uri('https://example.com/api/test_package/status');

        $request = $this->createMock(ServerRequest::class);
        $request->method('getUri')->willReturn($uri);

        $router = new Router($apiRouteConfigService);
        $route = $router->match($request);

        $this->assertSame(StatusApi::class, $route->getClass());
        $this->assertFalse($route->hasExt());
        $this->assertSame('test_package', $route->getPackage());
        $this->assertSame('/status', $route->getPath());
        $this->assertTrue($route->isPublic());
        $this->assertFalse($route->hasMimes());
        $this->assertSame(Api::TYPE_EARLY, $route->getType());
    }

    /**
     * @covers ::match
     */
    public function testRouterReturnsNullForNonMatchingRoute(): void
    {
        $routes = [
            '/status' => [
                'class' => StatusApi::class,
                'public' => true,
            ],
        ];

        $apiRouteConfigService = $this->createMock(ApiRouteConfigService::class);
        $apiRouteConfigService->method('getRoutesForPackage')->willReturn($routes);

        $uri = new Uri('https://example.com/api/test_package/enabled');

        $request = $this->createMock(ServerRequest::class);
        $request->method('getUri')->willReturn($uri);

        $router = new Router($apiRouteConfigService);
        $route = $router->match($request);

        $this->assertNull($route);
    }

    /**
     * @covers ::match
     */
    public function testRouterReturnsNullForUnknownPackage(): void
    {
        $apiRouteConfigService = $this->createMock(ApiRouteConfigService::class);
        $apiRouteConfigService->method('getRoutesForPackage')->willThrowException(new UnknownPackageException());

        $uri = new Uri('https://example.com/api/test_package/enabled');

        $request = $this->createMock(ServerRequest::class);
        $request->method('getUri')->willReturn($uri);

        $router = new Router($apiRouteConfigService);
        $route = $router->match($request);

        $this->assertNull($route);
    }

    /**
     * @covers ::match
     */
    public function testRouterReturnsNullForMalformedUrl(): void
    {
        $apiRouteConfigService = $this->createMock(ApiRouteConfigService::class);

        $uri = new Uri('https://example.com/_foo');

        $request = $this->createMock(ServerRequest::class);
        $request->method('getUri')->willReturn($uri);

        $router = new Router($apiRouteConfigService);
        $route = $router->match($request);

        $this->assertNull($route);
    }
}
