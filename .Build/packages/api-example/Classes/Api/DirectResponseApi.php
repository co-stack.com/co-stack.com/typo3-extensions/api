<?php

declare(strict_types=1);

namespace CoStack\ApiExample\Api;

use CoStack\Api\Api\Api;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;

class DirectResponseApi implements Api
{
    private ResponseFactoryInterface $responseFactory;

    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request)
    {
        $response = $this->responseFactory->createResponse(200, 'foo');
        $response->getBody()->write('Hello Response');
        return $response;
    }
}
