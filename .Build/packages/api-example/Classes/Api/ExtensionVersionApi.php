<?php

declare(strict_types=1);

namespace CoStack\ApiExample\Api;

use CoStack\Api\Api\Api;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use TYPO3\CMS\Core\Package\PackageManager;

use function array_key_exists;

class ExtensionVersionApi implements Api
{
    /**
     * @var PackageManager
     */
    private $packageManager;

    public function __construct(PackageManager $packageManager)
    {
        $this->packageManager = $packageManager;
    }

    public function handle(ServerRequestInterface $request): array
    {
        $params = $request->getQueryParams();
        if (!array_key_exists('extkey', $params)) {
            throw new RuntimeException('Missing extkey parameter');
        }
        $extkey = $params['extkey'];

        if (!$this->packageManager->isPackageAvailable($extkey)) {
            throw new RuntimeException('Package not available');
        }

        $package = $this->packageManager->getPackage($extkey);

        return [
            'version' => $package->getPackageMetaData()->getVersion(),
        ];
    }
}
