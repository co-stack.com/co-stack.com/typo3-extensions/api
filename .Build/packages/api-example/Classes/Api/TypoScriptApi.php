<?php

declare(strict_types=1);

namespace CoStack\ApiExample\Api;

use CoStack\Api\Api\Api;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\TypoScript\FrontendTypoScript;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class TypoScriptApi implements Api
{
    public function handle(ServerRequestInterface $request): array
    {
        $typo3Version = new Typo3Version();
        if (11 === $typo3Version->getMajorVersion()) {
            /** @var TypoScriptFrontendController $typoScriptFrontendController */
            $typoScriptFrontendController = $request->getAttribute('frontend.controller');
            return $typoScriptFrontendController->tmpl->setup;
        }
        /** @var FrontendTypoScript $typoScript */
        $typoScript = $request->getAttribute('frontend.typoscript');
        return $typoScript->getSetupArray();
    }
}
