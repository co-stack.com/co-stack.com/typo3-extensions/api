<?php

declare(strict_types=1);

namespace CoStack\ApiExample\Api;

use CoStack\Api\Api\Api;
use PDO;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class PageInfoApi implements Api
{
    public function handle(ServerRequestInterface $request): array
    {
        /** @var TypoScriptFrontendController $typoScriptFrontendController */
        $typoScriptFrontendController = $request->getAttribute('frontend.controller');
        $givenId = $typoScriptFrontendController->id;
        $query = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $query->select('*')
              ->from('pages')
              ->where($query->expr()->eq('uid', $query->createNamedParameter($givenId, PDO::PARAM_INT)))
              ->setMaxResults(1);
        $result = $query->executeQuery();
        return $result->fetchAssociative();
    }
}
