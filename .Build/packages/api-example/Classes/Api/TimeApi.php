<?php

declare(strict_types=1);

namespace CoStack\ApiExample\Api;

use CoStack\Api\Api\Api;
use Psr\Http\Message\ServerRequestInterface;

class TimeApi implements Api
{
    public function handle(ServerRequestInterface $request): array
    {
        return ['time' => time()];
    }
}
