<?php

declare(strict_types=1);

use CoStack\Api\Api\Api;
use CoStack\ApiExample\Api\DirectResponseApi;
use CoStack\ApiExample\Api\ExtensionVersionApi;
use CoStack\ApiExample\Api\PageInfoApi;
use CoStack\ApiExample\Api\StatusApi;
use CoStack\ApiExample\Api\TimeApi;
use CoStack\ApiExample\Api\TypoScriptApi;

return [
    '_default' => [
        'v1' => [
            'mimes' => [
                'application/json',
            ],
        ],
        'public' => [
            'public' => true,
        ],
    ],
    'v1/extension/version' => [
        'class' => ExtensionVersionApi::class,
    ],
    'public/status' => [
        'class' => StatusApi::class,
        'mimes' => [
            'application/xml',
            'application/json',
        ],
    ],
    'public/time' => [
        'class' => TimeApi::class,
    ],
    'typoscript' => [
        'class' => TypoScriptApi::class,
        'public' => false,
        'type' => Api::TYPE_FRONTEND,
    ],
    'fe/pageinfo' => [
        'class' => PageInfoApi::class,
        'public' => false,
        'type' => Api::TYPE_FRONTEND,
    ],
    'public/direct-response' => [
        'class' => DirectResponseApi::class,
    ],
];
