# TYPO3 API

[![pipeline status](https://gitlab.com/co-stack/typo3-extensions/api/badges/master/pipeline.svg)](https://gitlab.com/co-stack/typo3-extensions/api/pipelines)

Powered by co-stack.com

## About

This TYPO3 Extension adds a generic API capability to TYPO3.
The strength of this extension lies in its integration in TYPO3 itself and its ease of being used by other extensions.
If you want to create a fast and safe API in TYPO3 you should use this extension.

**There is no purpose in installing this extension without another extension that requires it.**

## Features

* Based on its own extendable PSR-15 request [Middleware](Documentation/Middlewares.md) stack.
* **Early** entrypoint: The API target is executed as soon as possible.
* **Frontend** entrypoint: The API target is executed after the TYPO3 frontend was booted. TypoScript etc. is available.
* Automatic response formatting into XML, JSON, CSV and more.

## Usage

Creating an API endpoint with EXT:api is simple and fast.
EXT:api and your extension must be installed for the following steps.

1. Create your API endpoint class. It can be anywhere but it is good practice to put it in `Classes/Api`.
   Name your class after its use case. (e.g. `Vendor\Packaga\Api\VersionApi`).
   Your API class must implement the interface `\CoStack\Api\Api\Api`.
   Return an array with some values to begin with.
1. Create your API registration file in `Configuration/Api.php` and register your API class.
   The file must return an associative array.
   The associative index is the `ROUTE`, or API endpoint name and will be part of the URL.
   Here is an example with the StatusApi class.
    ```php
    return [
       'status' => [
           'class' => \CoStack\Api\Api\StatusApi::class,
       ],
    ];
    ```
1. Login as administrator to your TYPO3 Backend. Go to the root page (ID 0) and select the list module.
   Create a new API token. Your registered API class should show up in the API scope list.
   Give your token a name and add your API endpoint to the list of APIs in scope.
   Safe the token to generate the token's secret.
1. You can test your API by using curl. Replace `SECRET`, `DOMAIN`, `EXTKEY` and `ROUTE` with your values:\
   `curl -H "Accept: application/json" -H "X-T3API-TOKEN: <SECRET>" https://<DOMAIN>/api/<EXTKEY>/<ROUTE>`\
   Example: `curl -H "Accept: application/json" -H "X-T3API-TOKEN: a220d5a473232e636f845e0f6919981a1baf13e97758d83b2bde7c5ec68954b7" https://local.myproject.de/api/api/status`
   You should see the array JSON encoded. (As of EXT:api 3.0 you can also use the GET parameter `token` instead of
   the `X-T3API-TOKEN` header)

## Advanced usage

### Automatic response formatting

Full documentation: [ResponseFormatting.md](Documentation/ResponseFormatting.md)

EXT:api tries to determine which content type was requested and formats the response accordingly. This spares you the
part of the identification and formatting of the response.

If you request `/api/api/status.xml` you will receive XML as response, if you request `/api/api/status.json`, you will
get the response as JSON string. Calling the API without a file extension `/api/api/status` can still send an `Accept`
header, which contains a list of prioritized mime types which should be returned. (Your browser typically sends
something like `text/html,application/xml;q=0.9,*/*;q=0.8`). EXT:api will try to find a formatter for the highest
prioritized mime type to format the response. If no `Accept` header was given, the mime type configured for the API will
be used.

![format-xml.png](Documentation/_img/format-xml.png)
![format-json.png](Documentation/_img/format-json.png)

### Additional middleware

Full documentation: [Middlewares.md](Documentation/Middlewares.md)

The EXT:api middleware stack is fully configurable.
Register your own middleware in `Configuration/RequestMiddlewares.php`:

```php
<?php

return [
    'api' => [
        'vendor/ext/request/logger' => [
            'target' => \Vendor\Ext\Middleware\Api\Logger::class,
            'before' => [
                'co-stack/api/router',
            ],
        ],
    ],
];
```

### API route configuration

Full documentation: [ApiRoutes.md](Documentation/ApiRoutes.md)

Creating APIs is very easy. Create the file `Configuration/Api.php` in your extension and register your API.
```php
<?php

declare(strict_types=1);

return [
    'v1/extension/version' => [
        'class' => \CoStack\Api\Api\ExtensionVersionApi::class,
    ],
];
```
Replace `EXT_KEY` with the extension key of your extension.

Call the API: `https://host.tld/api/EXT_KEY/v1/extension/version`

## Debugging

To develop and debug your API calls you should set TYPO3's `displayErrors` to wither `1` for local development.
In public or production environments `displayErrors` should be `-1` and your `devIPmask` should be set.
Enabling this setting will show exception instead of failing silently.

### curl

Your API responses may be empty if something went wrong.
You can add following options to your curl call to get more information:

* **`-I`**: Display the response headers (especially useful if you do not see any response).
* **`-H "X-T3API-SAPI: cli"`**: Force the exception to be rendered for CLI.

Following HTTP Status codes are currently implemented:

* **403**: You did not include an authentication header (`X-T3API-TOKEN`) or the provided secret is incorrect.
* **406**: You did not include an `accept` header in your request or the accepted mime is not supported.
  Always try with `application/json` first.
* **429**: You hit the hard rate limit. Increase the requests per minute in the extension settings.
  You can set this to `0` to disable the rate limiter, but you must be aware of security implications (e.g. DoS).
* **500**: An exception occurred in your API class. Check the logs.

### PhpStorm

When you do not get the results you expect from an API call you can debug requests with xDebug and PhpStorm.

**Please use the internet search if you have any questions about xDebug or PhpStorm. We will not answer inquiries on
these topics.**

1. Create a new .http scratch file. Replace `SECRET`, `DOMAIN`, `EXTKEY` and `ROUTE` with your values):
    ```http request
    https://<DOMAIN>/api/<EXTKEY>/<ROUTE>
    Accept: application/json
    X-T3API-TOKEN: <SECRET>
    ```
1. Add a stop point in `\CoStack\Api\Middleware\Frontend\EarlyApiMiddleware::process`.
1. Click on the green arrow left of the URL and select PHP Debug.
1. If your project is set up correctly it should hit your stop point.
