<?php

/**
 * @var array $EM_CONF
 * @var string $_EXTKEY
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'co-stack.com API',
    'description' => 'TYPO3 REST API foundation built using middleware',
    'category' => 'services',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Oliver Eglseder',
    'author_email' => 'oliver.eglseder@co-stack.com',
    'author_company' => 'co-stack.com',
    'version' => '3.0.0',
    'constraints' => [
        'depends' => [
            'php' => '8.1.0-8.99.99',
            'typo3' => '11.5.0-12.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
