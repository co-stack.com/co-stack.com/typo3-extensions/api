<?php

declare(strict_types=1);

use CoStack\Api\Backend\Token\TcaApiScopeItemsProvider;

return [
    'columns' => [
        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.name',
            'description' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.name.description',
            'config' => [
                'type' => 'input',
                'eval' => 'trim, unique',
                'max' => 191,
                'placeholder' => 'Discovery API access for surveillance system',
            ],
        ],
        'secret' => [
            'exclude' => true,
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.secret',
            'description' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.secret.description',
            'config' => [
                'type' => 'input',
                'readOnly' => true,
            ],
        ],
        'user_comment' => [
            'exclude' => true,
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.user_comment',
            'description' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.user_comment.description',
            'config' => [
                'type' => 'text',
                'eval' => 'trim',
                'max' => 2000,
            ],
        ],
        'disabled' => [
            'exclude' => true,
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.disabled',
            'description' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.disabled.description',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true,
                    ],
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.starttime',
            'description' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.starttime.description',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.endtime',
            'description' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.endtime.description',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038),
                ],
            ],
        ],
        'request_cidr' => [
            'exclude' => true,
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.request_cidr',
            'description' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.request_cidr.description',
            'config' => [
                'type' => 'input',
                'eval' => 'trim, required',
                'placeholder' => '10.0.1.0/24, 127.0.0.1/24, ::1',
            ],
        ],
        'scope' => [
            'exclude' => true,
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.scope',
            'description' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.column.scope.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'allowNonIdValues' => true,
                'itemsProcFunc' => TcaApiScopeItemsProvider::class . '->getItems',
                'minitems' => 1,
                'maxitems' => 5,
            ],
        ],
    ],
    'ctrl' => [
        'adminOnly' => true,
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'default_sortby' => 'name',
        'delete' => 'deleted',
        'descriptionColumn' => 'user_comment',
        'enablecolumns' => [
            'disabled' => 'disabled',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'iconfile' => 'EXT:api/Resources/Public/Icons/tx_api_domain_model_token.svg',
        'label' => 'name',
        'rootLevel' => true,
        'searchFields' => 'name, user_comment',
        'title' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.ctrl.title',
        'tstamp' => 'tstamp',
    ],
    'interface' => [],
    'palettes' => [
        'expiration_limit' => [
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.palettes.expiration_limit',
            'showitem' => '
                disabled,
                starttime,
                endtime,
            ',
        ],
        'request_limit' => [
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.palettes.request_limit',
            'showitem' => '
                request_cidr,
            ',
        ],
        'scope_limit' => [
            'label' => 'LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.palettes.scope_limit',
            'showitem' => '
                scope,
            ',
        ],
    ],
    'types' => [
        '1' => [
            'showitem' => '
                --div--;LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.tabs.basic,
                    name,
                    secret,
                    --palette--;;scope_limit,
                --div--;LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.tabs.access,
                    --palette--;;expiration_limit,
                    --palette--;;request_limit,
                --div--;LLL:EXT:api/Resources/Private/Language/locallang_token.xlf:tx_api_domain_model_token.tabs.additional,
                    user_comment,
            ',
        ],
    ],
];
