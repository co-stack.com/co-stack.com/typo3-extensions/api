<?php

declare(strict_types=1);

use CoStack\Api\Middleware\Api\ApiExecutorMiddleware;
use CoStack\Api\Middleware\Api\AuthorizationMiddleware;
use CoStack\Api\Middleware\Api\GeneralRateLimiterMiddleware;
use CoStack\Api\Middleware\Api\ResponseFormatterMiddleware;
use CoStack\Api\Middleware\Api\RouterMiddleware;
use CoStack\Api\Middleware\Frontend\EarlyApiMiddleware;
use CoStack\Api\Middleware\Frontend\FrontendApiMiddleware;

return [
    'frontend' => [
        'co-stack/api/early' => [
            'target' => EarlyApiMiddleware::class,
            'before' => [
                'typo3/cms-frontend/eid',
            ],
            'after' => [
                'typo3/cms-core/normalized-params-attribute',
            ],
        ],
        'co-stack/api/frontend' => [
            'target' => FrontendApiMiddleware::class,
            'after' => [
                'typo3/cms-core/response-propagation',
            ],
        ]
    ],
    'api' => [
        'co-stack/api/router' => [
            'target' => RouterMiddleware::class,
        ],
        'co-stack/api/ratelimiter' => [
            'target' => GeneralRateLimiterMiddleware::class,
            'before' => [
                'co-stack/api/router',
            ],
        ],
        'co-stack/api/authorization' => [
            'target' => AuthorizationMiddleware::class,
            'after' => [
                'co-stack/api/router',
            ],
            'before' => [
                'co-stack/api/api/executor',
            ],
        ],
        'co-stack/api/api/executor' => [
            'target' => ApiExecutorMiddleware::class,
            'after' => [
                'co-stack/api/router',
            ],
        ],
        'co-stack/api/response/formatter' => [
            'target' => ResponseFormatterMiddleware::class,
            'after' => [
                'co-stack/api/router',
                'co-stack/api/api/executor',
            ],
        ],
    ],
];
