<?php

declare(strict_types=1);

use CoStack\Api\Api\DiscoveryApi;
use CoStack\Api\Api\StatusApi;

return [
    '/status' => [
        'class' => StatusApi::class,
        'public' => true,
    ],
    '/discovery' => [
        'class' => DiscoveryApi::class,
    ],
];
