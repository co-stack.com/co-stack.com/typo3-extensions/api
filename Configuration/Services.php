<?php

declare(strict_types=1);

namespace CoStack\Api;

use CoStack\Api\Api\Api;
use CoStack\Api\DependencyInjection\ResponseFormatterPass;
use CoStack\Api\ResponseFormatter\ResponseFormatter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Core\DependencyInjection\PublicServicePass;

return static function (ContainerConfigurator $container, ContainerBuilder $containerBuilder): void {
    $containerBuilder->registerForAutoconfiguration(Api::class)->addTag('co-stack.api');
    $containerBuilder->addCompilerPass(new PublicServicePass('co-stack.api'));

    $containerBuilder->registerForAutoconfiguration(ResponseFormatter::class)->addTag('co-stack.response.formatter');
    $containerBuilder->addCompilerPass(new ResponseFormatterPass('co-stack.response.formatter'));
};
