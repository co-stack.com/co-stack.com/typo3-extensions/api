CREATE TABLE tx_api_domain_model_token
(
    name         varchar(191)             NOT NULL,
    secret       char(64)                 NOT NULL,
    user_comment varchar(2000) DEFAULT '' NOT NULL,
    request_cidr varchar(255)             NOT NULL,
    scope        varchar(2000)            NOT NULL,

    UNIQUE INDEX `name` (`name`),
    UNIQUE INDEX `secret` (`secret`)
);
