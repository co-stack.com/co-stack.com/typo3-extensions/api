<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}
(static function (): void {
    if (empty($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_api_fastcache'])) {
        $backend = \TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend::class;
        if (extension_loaded('apcu')) {
            $backend = \TYPO3\CMS\Core\Cache\Backend\ApcuBackend::class;
        } elseif (extension_loaded('apc')) {
            $backend = \TYPO3\CMS\Core\Cache\Backend\ApcBackend::class;
        }
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['tx_api_fastcache'] = [
            'backend' => $backend,
        ];
    }

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][1584579385] = \CoStack\Api\Backend\Token\TcaTokenSecretGenerator::class;
})();
