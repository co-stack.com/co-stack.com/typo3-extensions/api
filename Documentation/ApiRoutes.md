# API Routes

Each API endpoint has a set of configuration options.
This example shows the full set of configuration options:

```php
<?php

declare(strict_types=1);

return [
    '_default' => [
        'v1' => [
            'mimes' => [
                'application/json',
            ],
        ],
        'public' => [
            'public' => true,
        ],
    ],
    'v1/extension/version' => [
        'class' => \CoStack\Api\Api\ExtensionVersionApi::class,
    ],
    'v1/file/info' => [
        'class' => \CoStack\Api\Api\FileInfoApi::class,
    ],
    'v1/status' => [
        'class' => \CoStack\Api\Api\StatusApi::class,
        'public' => true,
    ],
    'v1/discovery' => [
        'class' => \CoStack\Api\Api\DiscoveryApi::class,
        'mimes' => [
            'application/xml',
        ],
    ],
    'public/discovery' => [
        'class' => \CoStack\Api\Api\PublicDiscoveryApi::class,
        'mimes' => [
            'application/xml',
            'application/json',
        ],
    ],
    'frontend/pageinfo' => [
        'class' => \CoStack\Api\Api\PageInfoApi::class,
        'type' => \CoStack\Api\Api\Api::TYPE_FRONTEND,
    ],
];
```

Explanation:

* `class`: The FQCN of your class that implements the API.
* `public`: Routes which are public don't require the `X-T3API-TOKEN` header. They are always available.
* `mimes`: Define default mime types for the response formatting.
* `type`: One of the `\CoStack\Api\Api\Api::TYPE_` constants. Default is `TYPE_EARLY`, which calls the API as early as
  possible. Use `TYPE_FRONTEND` to dispatch the API call when the frontend booted and TSFE and TypoScript is available.
* `_default`: The `_default` key is reserved to define defaults for all routes matching the sub-keys.
  In this example `_default` contains `v1` and `v2`. All routes beginning with `v1` will inhert the `mimes` setting.
  The `v2` defaults contains `'public' => true` which will make all routes starting with `public` publicly available.

## Entrypoints

### Early Entrypoint (default)

The be as fast as possible, APIs skip everything frontend rendering related. This still allows you to use TYPO3 specific
elements like the compiled service container, TYPO3 classes like `Environment` or the database connection.

You can not use anything frontend related like TypoScript, page rendering, site configurations, etc. If you want to use
frontend related features, you need to define the frontend type for your API.

### Frontend Entrypoint

A frontend entrypoint is defined by adding the `type` attribute and setting it to `\CoStack\Api\Api\Api::TYPE_FRONTEND`.

```php
<?php

declare(strict_types=1);

return [
    'frontend/pageinfo' => [
        'class' => \CoStack\Api\Api\PageInfoApi::class,
        'type' => \CoStack\Api\Api\Api::TYPE_FRONTEND,
    ],
];
```

The API target (your class) will be called after the TypoScriptFrontendController was fully booted.

Caveats:

* **Frontend API requests require a page id**. Since TYPO3 can either route an API or a page request, you have to tell TYPO3
  the page ID via `id` GET parameter: `/api/my_ext/frontend/pageinfo?id=5151`. In this example TYPO3 will build the
  Frontend environment and TypoScript for the page `5151`. Obviously, the page must exist and be accessible.
* Frontend API requests are always uncached. This will populate the TSFE's `pSetup` and allows you to
  access `TYPO3\CMS\Core\TypoScript\FrontendTypoScript` via `$request->getAttribute('frontend.typoscript')`, but it
  slows down your API call.
