# Response Formatting

If an API returns an array instead of a response object, EXT:api will try to format the array according to the incoming
request. To choose the right formatter, EXT:api tries to determine the requested content type.

**Limitations**: Not all content types support arbitrary deep nested arrays. CSV does only support a simple flat array.

## Content Type determination

Responses are always formatted based on certain criteria. Currently, there are 3 criteria which can determine the
response content type. They are evaluated in the following order:

1. URL file extension: Calling an API url with `.json` or `.xml` at the end will force the response to be in that
   format.
2. Accept header: Accept headers can contain multiple mime types. They will be ordered by priority and the first mime
   type which is supported will be used (see Formatter).
3. Route defaults: Routes can define a default mime type that will be used when there is no file extension or accept
   header. Multiple mime types are tried in order until the first supported is found  (see Formatter).

## Formatter

Formatter are classes that can convert a PHP array into another mime type.
The most used Formatter is `\CoStack\Api\Core\Response\Formatter\JsonFormatter`.

Register your own formatter by simply implementing the `\CoStack\Api\Core\Response\Formatter\ResponseFormatter`
interface or rather extend `\CoStack\Api\Core\Response\Formatter\AbstractFormatter`:

```php
<?php

namespace Vendor\Ext\Api\Response\Formatter;

class SerializationFormatter extends \CoStack\Api\ResponseFormatter\AbstractFormatter
{
    protected $supportedMimes = [
        'application/php-serialized',
    ];

    public function process(array $data): string
    {
        return serialize($data);
    }
}
```

## Known Issues

* Exporting TypoScript or other arrays with integer keys followed by non `[:alphanum:]` characters (dots, slashes, etc.) as
  XML will create invalid XML with integer keys. The problem lies in the function used to convert to XML, which is the
  TYPO3 core function `\TYPO3\CMS\Core\Utility\GeneralUtility::array2xml`.
