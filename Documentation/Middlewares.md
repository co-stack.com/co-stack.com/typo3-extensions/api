# API Middlewares

EXT:api uses a frontend middleware to divert the execution path, when an API path was
requested. (https://host.tld/api/...). **Early** API requests will not go further through the frontend middleware stack.
Instead, the API middleware stack will be used to dispatch the request to the API target. **Frontend** API requests will
remain in the frontend middleware stack until `typo3/cms-core/response-propagation` and then divert into the API
middleware stack. Regardless of the entrypoint (**early** or **frontend**) the API middleware stack is always used.

You can see the registered API middlewares in the TYPO3 Configuration Module (package `typo3/cms-lowlevel` required).

![middleware-stack.png](_img%2Fmiddleware-stack.png)

## Extend the API Middlewares.

Extending the API middleware stack follows the same principles as for the frontend or backend middelwares. You register
a middleware at the key `api`.

```php
<?php

declare(strict_types=1);

return [
    'api' => [
        'vendor/package/middleware-identifier' => [
            'target' => \Vendor\Package\MyMiddleware::class,
        ],
    ],
];
```

See https://docs.typo3.org/m/typo3/reference-coreapi/12.4/en-us/ExtensionArchitecture/FileStructure/Configuration/RequestMiddlewaresPhp.html
